package it.unibo.oop.lab.nesting2;

import java.util.*;

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private final List<T> list;
	final Iterator<T> iterator;

	public OneListAcceptable(List<T> list) {
		super();
		this.list = list;
		iterator = this.list.iterator();
	}

	@Override
	public Acceptor<T> acceptor() {
		
		return new Acceptor<T>(){

			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				if(iterator.hasNext()) {
					if(iterator.next() == newElement) {
						return;
					}
				}
				throw new ElementNotAcceptedException(newElement);
			}

			@Override
			public void end() throws EndNotAcceptedException {
				if(iterator.hasNext()) {
					throw new EndNotAcceptedException();
				}
			}
			
		};
	}

}
